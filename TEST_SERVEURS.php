#!/usr/bin/php -q
<?php

#============================ MOD CLI ============================

if ($argc != 3)
{
    echo "il manque un argument, veuillez lire le README pour plus d'informations.\n";
}
else
{
    
#Définit le fuseau horaire par défaut à utiliser.
date_default_timezone_set('Europe/Paris');
$date = date('l jS \of F Y h:i:s A');
   echo "============================== DEBUT ==============================\n\n";
   echo "============ $date =============\n\n";
   
   $string="0 received";
   
   $file = file_get_contents('./liste.txt'); #contenu du fichier liste (adresses ip)
   
   $lines=file('liste.txt'); #variable correspondant au contenu du fichier 
   
   $commande = "ping -w 1 -c 1 ";
   
   $cpterreur = 0;
 
   foreach ($lines as $line_num => $line)
        {
        $a=shell_exec($commande . "$line");
   
    if (strpos($a, $string) !== false)
        {
        $result="\033[31m[ NOT OK ]\033[0m";
        echo "$result " . "=> $line" . "\n" ; 
        $cpterreur++;
        }
    else
        {
        $result="\033[32m[ OK ]\033[0m";
        echo "$result " . "=> $line" . "\n" ;
        }
        }
        
   echo "==============================  FIN  ==============================\n\n";
   
#============================ MOD HTML ============================

$pageHtml = "
<!DCOTYPE html>
<html lang='fr'>
<head>
    <title> TestPing </title>


</head>

<body>
    <p> Ping des adresses IP : </p>
    <table>
        <tr>
            <td> 10.10.9.175 </td>
            <td> $result </td>
        </tr>
        <tr>
            <td> 10.10.9.176 </td>
            <td> $result </td>
        </tr>
        <tr>
            <td> 10.10.9.177 </td>
            <td> $result </td>
        </tr>
        <tr>
            <td> 10.10.9.195 </td>
            <td> $result </td>
        </tr>
        <tr>
            <td> 10.10.9.196 </td>
            <td> $result </td>
        </tr>
        <tr>
            <td> 10.10.9.199 </td>
            <td> $result </td>
        </tr>
        <tr>
            <td> 8.8.8.8 </td>
            <td> $result </td>
        </tr>
    </table>
</body>
</html>
";

$newFile = 'status';

$open = fopen('/home/etudiant1/Bureau/hubertlea/miniprojetphp/'.$newFile.'.html','w'); #ouverture du fichier html la ou il doit être ouvert et créé

fwrite($open,$pageHtml); #ecriture dans ce dernier
fclose($open); #fermeture de la modification 

#============================ MOD MAIL ============================

// Adr mail destinataire
$to = "l.hubert508@outlook.fr";

// Sujet du courriel
$subject = utf8_decode("Surveillance réseau SIO2-SLAM : Erreur(s) détectée(s)");

// L'en-tête Mail "Content-type" doit être défini :
$headers = "MIME-Version: 1.0 \r\n";
$headers .= "Content-type: text/html; charset=UTF-8 \r\n";

// En-têtes additionnels :
$headers .= "To: l.hubert508@outlook.fr\r\n";
$headers .= "From: lea.new2@orange.fr\r\n";

// Envoi du mail
if (mail($to, $subject, $pageHtml, $headers)) echo "\n=> Courriel envoyé !!\n";
else echo "\n=> Erreur dans l'envoi du courriel !!\n";

}

#============================ MOD SQL ============================

//Vérification de connexion aux serveurs mysql
$serveur = 'localhost';
$utilisateur = 'lea';
$motdepasse = 'newton08090';
            
//début de la connexion
$conn = new mysqli($serveur, $utilisateur, $motdepasse);
            
//vérification
if($conn->connect_error)
    {
    echo "\n-------------------------------------------------------------------\n\nerreur de connexion lors de la connexion aux serveur mysql\n\n";
    }
    echo "\n-------------------------------------------------------------------\n\nConnexion réussie aux serveurs mysql\n\n";

//création d'une base de données contenant les adresses IP
try
    {
    $dbco = new PDO("mysql:host=$serveur", $utilisateur, $motdepasse); #information pour se connecter à sql
    $dbco->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); #définition des attributs
    
    $sql1 = "DROP DATABASE IF EXISTS listeIP"; #supprime la database actuelle pour pouvoir en re créer une et ainsi elle sera toujours à jour
    $sql2 = "CREATE DATABASE listeIP";
    $sql3 = "USE listeIP";
    $sql4 = "CREATE TABLE ADRESSESIP (
        ID_IP VARCHAR(15),
        DATE_DERNIER_CONTROL DATE,
        NOMBRE_DE_ERREUR INTEGER,
        PRIMARY KEY (ID_IP)
        )";
    
    #ajout des adresses IP dans la table
    $sql5 = "INSERT INTO ADRESSESIP VALUES (
        '10.10.9.175',CURDATE(),$cpterreur)";
    $sql6 = "INSERT INTO ADRESSESIP VALUES (
        '10.10.9.176',CURDATE(),$cpterreur)";
    $sql7 = "INSERT INTO ADRESSESIP VALUES (
        '10.10.9.177',CURDATE(),$cpterreur)";
    $sql8 = "INSERT INTO ADRESSESIP VALUES (
        '10.10.9.195',CURDATE(),$cpterreur)";
    $sql9 = "INSERT INTO ADRESSESIP VALUES (
        '10.10.9.196',CURDATE(),$cpterreur)";
    $sql10 = "INSERT INTO ADRESSESIP VALUES (
        '10.10.9.199',CURDATE(),$cpterreur)";
    $sql11 = "INSERT INTO ADRESSESIP VALUES (
        '8.8.8.8',CURDATE(),$cpterreur)";
        
    #execute les commandes précédentes dans mysql
    $dbco->exec($sql1);
    $dbco->exec($sql2);
    $dbco->exec($sql3);
    $dbco->exec($sql4);
    $dbco->exec($sql5);
    $dbco->exec($sql6);
    $dbco->exec($sql7);
    $dbco->exec($sql8);
    $dbco->exec($sql9);
    $dbco->exec($sql10);
    $dbco->exec($sql11);
    
                
    echo "Base de données créée avec succès\n\n"; #ecrit cela a la fin du lancement du script afin de vérifier si celui ci a bien fonctionné
    }
        catch(PDOException $e)              
        {
         echo "Erreur : " . $e->getMessage() . "\n\n"; #erreur si ca ne marche pas et renvois à l'utilisateur un message sur l'origine de l'erreur
        }


$conn->close(); #fermeture de la connexion une fois son utilisation terminée
    
?>
