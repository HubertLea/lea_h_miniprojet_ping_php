## LEA HUBERT - Mini Projet Ping (php)

----------------------------------------------------
#### commande à effectuer pour appeller le programme 


Dans l'invite de commande :

./TEST_SERVEURS.php liste.txt status.html

----------------------------------------------------
#### Informations sur le script TEST_SERVEURS.php


C'est à partir de ce script que tout est créé.

En effet, vous pourrez retrouver plusieurs partie dans un même script.

----------------------------------------------------
#### Informations sur status.html


Créer dans TEST_SERVEURS.php

Il sera automatiquement actualisé à chaque lancement du script.

----------------------------------------------------
#### Informations sur la base de données


Une base de donnée est créée à partir de ce script et est actualisé à chaque lancement de ce dernier.

----------------------------------------------------
#### Informations sur l'envois de mail


Un mail est envoyé à une adresse mail au choix à chaque lancement du script.

Or, il s'avère que cette dernière fonction fonctionne mal pour le moment.

----------------------------------------------------
#### Informations sur la créatrice de ce script


e-mail : lea.new2@orange.fr

Téléphone : 07.81.20.64.14
